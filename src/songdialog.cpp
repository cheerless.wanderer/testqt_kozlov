#include "songdialog.h"
#include "src/tablerow.h"
#include "ui_songdialog.h"
#include <QFileDialog>

SongDialog::SongDialog(QWidget* parent): QDialog(parent), m_ui(new Ui::SongDialog())
{
	m_ui->setupUi(this);
	connect(m_ui->btnPreview, &QPushButton::clicked, this, &SongDialog::choosePreview);
	QDir dir;
	if (!dir.exists(m_imageDir))
		dir.mkpath(m_imageDir);
}

SongDialog::~SongDialog()
{
    delete m_ui;
}


void SongDialog::choosePreview()
{
	const auto filename = QFileDialog::getOpenFileName(this, tr("Выберите изображение"),
		QString(), "(*.jpeg *.jpg *png *gif)");
	if (filename.isEmpty())
		return;
	const QString copyName = copiedPreview(filename);
	showPreview(copyName);
}

QString SongDialog::copiedPreview(const QString& filePath) const
{

	const QImage image(filePath);
	const auto smaller = image.scaled(m_ui->labelPreview->width(), m_ui->labelPreview->height(),
		Qt::AspectRatioMode::KeepAspectRatioByExpanding);

	QString name = "1.png";
	if (!smaller.save(fullImagePath(name), "PNG"))
		return QString();

	return  name;
}

void SongDialog::showPreview(const QString& relativePath) const
{
	if (relativePath.isEmpty())
		return;
	const auto fn = fullImagePath(relativePath);
	if (!QFile::exists(fn))
		return;

	const QPixmap pixmap(fn);	
	m_ui->labelPreview->setPixmap(pixmap);
}


QString SongDialog::fullImagePath(const QString& relativePath) const
{
	QDir dir;
	dir.cd(m_imageDir);
	return dir.absoluteFilePath(relativePath);
}

TableRow SongDialog::rowWithData()
{
    return rowForInsert;
}

void SongDialog::on_buttonBox_accepted()
{
    // Заполняем структуру данных(ряд таблицы) для передачи данных в таблицу
    TableRow row;

    // Проверяем изображение
    if (m_ui->labelPreview->pixmap() != nullptr)
    {
        QPixmap originalPixmap = m_ui->labelPreview->pixmap()->copy();
        QPixmap scaledPixmap = originalPixmap.scaled(60, 75, Qt::KeepAspectRatio);
        m_ui->labelPreview->setPixmap(scaledPixmap);
        rowForInsert.albumPreview = scaledPixmap;
    }
    else
    {
        QMessageBox::critical(this, "Ошибка", "Пожалуйста, добавьте изображение");
        return;
    }

    // Проверяем название песни
    QString title = m_ui->nameText->text();
    if (title.isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Пожалуйста, введите название песни");
        return;
    }
    rowForInsert.title = title;

    // Проверяем имя исполнителя
    QString artist = m_ui->singerText->text();
    if (artist.isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Пожалуйста, введите имя исполнителя");
        return;
    }
    rowForInsert.artist = artist;

    // Проверяем продолжительность
    QString duration = m_ui->timeText->text();
    if (duration.isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Пожалуйста, введите продолжительность песни");
        return;
    }
    rowForInsert.duration = duration;

    // Оценка
    rowForInsert.rating = m_ui->ratingSlider->value();

    QMessageBox::information(this, "Успешно", "Данные успешно добавлены в таблицу");
}


// Метод для получения данных для редактирования из онка с таблицей
void SongDialog::setEditSong(TableRow row, int currentIndex)
{
    m_ui->labelPreview->setPixmap(row.albumPreview);
    m_ui->nameText->setText(row.title);
    m_ui->singerText->setText(row.artist);
    m_ui->timeText->setText(row.duration);
    m_ui->ratingSlider->setValue(row.rating);

}
