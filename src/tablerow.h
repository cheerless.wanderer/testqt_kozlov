// tablerow.h

#ifndef TABLEROW_H
#define TABLEROW_H

#include <QString>
#include <QPixmap>

// Структура данных для ряда таблицы
struct TableRow {
    QPixmap albumPreview; // Превью альбома (изображение)
    QString title;        // Название песни
    QString artist;       // Исполнитель
    QString duration;     // Продолжительность
    int rating;           // Оценка (целое число)
};

#endif // TABLEROW_H
