#include "songlistdialog.h"
#include "qdebug.h"
#include "src/songdialog.h"
#include "ui_songlistdialog.h"
#include <QStandardItemModel>
#include <QFile>
#include <QTextStream>

SongListDialog::SongListDialog(QWidget *parent): QDialog(parent)
{
    m_ui = new Ui::SongListDialog();
    m_ui->setupUi(this);

    m_model = new DataModel(this);
    proxyModel = new QSortFilterProxyModel(this); //Создание прокси-модели для корректной сортировки

    loadTableData(fileName);

    //Устанавливаем источником для прокси-модели модель данных
    proxyModel->setSourceModel(m_model);
    //Устанавливаем прокси-модель в качестве модели для таблицы
    m_ui->tableSongList->setModel(proxyModel);

    //Устанавливаем растягивание столбцов по содержимому
    m_ui->tableSongList->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    m_ui->tableSongList->setEditTriggers(QAbstractItemView::NoEditTriggers);

    m_ui->tableSongList->hideColumn(4);

}

SongListDialog::~SongListDialog()
{
    delete proxyModel; // Уничтожение прокси-модели
    delete m_ui;
}

// Обработчик события кнопки "Создать"
void SongListDialog::on_btnCreate_clicked()
{
    SongDialog dlg;

    if(dlg.exec())
    {
        TableRow insertRow = dlg.rowWithData();
        if (insertRow.albumPreview.isNull() || insertRow.title.isEmpty() || insertRow.artist.isEmpty() || insertRow.duration.isEmpty())
            return;

        m_currentRowIndex = m_model->rowCount(QModelIndex());
        m_model->insertRow(m_currentRowIndex, 1, insertRow, QModelIndex());
    }

}


// Обработчик события для кнопки "Редактировать"
void SongListDialog::on_btnEdit_clicked()
{
    // Получаем индекс выделенной строки в прокси-модели
            QModelIndexList selectedIndexes = m_ui->tableSongList->selectionModel()->selectedIndexes();

    if (selectedIndexes.isEmpty()) {
        // Обработка ошибки: ни одна строка не выделена
        return;
    }

    // Получаем индекс выделенной строки в основной модели
    QModelIndex sourceIndex = proxyModel->mapToSource(selectedIndexes.first());
    m_currentRowIndex = sourceIndex.row();
    if (!sourceIndex.isValid()) {
        // Обработка ошибки: индекс не является допустимым
        return;
    }

    //Получаем данные о выбранном ряде модели данных
    TableRow songData = static_cast<DataModel*>(m_model)->getRow(sourceIndex);
    SongDialog dlg;
    QString m_name = songData.title;
    dlg.setEditSong(songData, m_currentRowIndex);
    m_name = songData.title;
    if(dlg.exec())
    {
        TableRow insertRow = dlg.rowWithData();
        if (insertRow.albumPreview.isNull() || insertRow.title.isEmpty() || insertRow.artist.isEmpty() || insertRow.duration.isEmpty())
            return;

        m_model->editRow(m_currentRowIndex, 1, insertRow);
    }
}

// Обработчик события кнопки "Сортировать"
void SongListDialog::on_btnSort_clicked()
{
    // Устанавливаем сортировку по четвертому столбцу в порядке убывания
    proxyModel->sort(4, Qt::DescendingOrder);
}


// Обработчик события кнопки "Сохранить"
void SongListDialog::on_btnSave_clicked()
{
    QString filePath = QString("./") + fileName;
    saveTableData(filePath);
}

// Сохранение таблицы в файл
void SongListDialog::saveTableData(const QString& fileName)
{
    QString filePath = QString("./") + fileName;
    if (m_model->saveTableData(filePath)) {
        qDebug() << "Table data saved successfully!";
    } else {
        qDebug() << "Error saving table data!";
    }
}

// Загрузка данных таблицы из файла
void SongListDialog::loadTableData(const QString &groupName)
{
    if (!m_model->loadTableData(groupName))
    {
        // Обработка ошибки загрузки данных
        qDebug() << "Failed to load data from file";
    }
}
