#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QAbstractTableModel>
#include <QList>
#include <QPixmap>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QBuffer>
#include <QFile>
#include <QDataStream>
#include <QDebug>
#include "tablerow.h"
#include "song.h"
class DataModel : public QAbstractTableModel
{
    Q_OBJECT
private:
    QList<Song> songs;
    QStringList headers_;

    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;


public:
    DataModel(QObject *parent);

    // Model interface implementation
    TableRow getRow(const QModelIndex &index) const;
    virtual bool insertRow(int row, int count, TableRow row1, const QModelIndex &parent);
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    bool saveTableData(const QString &fileName);
    bool loadTableData(const QString &fileName);
    void saveTableRowData(QSqlQuery& query, int row);
    bool editRow(int row, int count, TableRow row1);
    virtual QVariant data(const QModelIndex &index, int role) const override;

};
#endif // DATAMODEL_H
