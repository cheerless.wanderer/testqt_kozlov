// song.h

#ifndef SONG_H
#define SONG_H

#include "src/tablerow.h"
#include <QPixmap>
#include <QString>

class Song {
public:
    Song(const QPixmap& preview, const QString& name, const QString& artist, const QString& duration, int rating);

    QPixmap preview() const;
    QString name() const;
    QString artist() const;
    QString duration() const;
    int rating() const;
    int id() const;
    void setValues(TableRow row);

private:
    QPixmap m_preview;
    QString m_name;
    QString m_artist;
    QString m_duration;
    int m_rating;
    int m_ID;
};

#endif // SONG_H
