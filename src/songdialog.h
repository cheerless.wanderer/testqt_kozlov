#pragma once
#include <QDialog>
#include "tablerow.h"
#include <QMessageBox>

namespace Ui 
{	
class SongDialog;
}

class SongDialog : public QDialog
{
	Q_OBJECT
public:
    SongDialog(QWidget* parent = nullptr);
	~SongDialog();
    TableRow rowWithData();
    TableRow rowForInsert;
    void setEditSong(TableRow editRow, int currentIndex);


private:
	void choosePreview();
	QString copiedPreview(const QString& filePath) const;
	void showPreview(const QString& relativePath) const;
	QString fullImagePath(const QString& relativePath) const;
	Ui::SongDialog* m_ui = nullptr;
	const QString m_imageDir = "../../images";

private slots:
    void on_buttonBox_accepted();
};

