#include "datamodel.h"
#include "qapplication.h"
#include "qdebug.h"

DataModel::DataModel(QObject *parent)
    : QAbstractTableModel{parent}
{
    // Задаём заголовки
    headers_.append("Превью");
    headers_.append("Название");
    headers_.append("Исполнитель");
    headers_.append("Продолжительность");
    headers_.append("Оценка");
}

// Метод для подсчёта количества столбцов
int DataModel::columnCount(const QModelIndex &) const
{
    return 5;
}

// Метод для подсчёта количества рядов
int DataModel::rowCount(const QModelIndex &parent) const
{
    return this->songs.size();
}

// Метод для представления информации о каждой ячейке
QVariant DataModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();
    int column = index.column();

    if(role == Qt::DecorationRole && column == 0)
    {

        return songs[row].preview();
    }

    if(role == Qt::DisplayRole)
    {
        // В зависимости от столбца, возвращаем соответствующее значение
        switch (column) {
        case 1:
            return songs[row].name();
        case 2:
            return songs[row].artist();
        case 3:
            return songs[row].duration();
        case 4:
            return songs[row].rating();
        }
    }

    if(role == Qt::ToolTipRole)
    {

        return QVariant("Оценка: " + QString::number(songs[row].rating()));
    }

    if (role == Qt::TextAlignmentRole) {
        // Устанавливаем выравнивание по центру для всех столбцов
        return Qt::AlignCenter;
    }

    return QVariant();
}

// Метод для установки заголовков
QVariant DataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QAbstractTableModel::headerData(section, orientation, role);

    if (orientation == Qt::Orientation::Horizontal) {
        return this->headers_[section];
    }

    return QAbstractTableModel::headerData(section, orientation, role);
}

//Метод для добавления ряда таблицы
bool DataModel::insertRow(int row, int count, TableRow rowForInsert, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row);

    songs.insert(row,Song(rowForInsert.albumPreview,rowForInsert.title,rowForInsert.artist,rowForInsert.duration,rowForInsert.rating));

    endInsertRows();
    return true;
}



//Метод получения выделенного ряда
TableRow DataModel::getRow(const QModelIndex &index) const
{
    if (!index.isValid())
        return TableRow();

    int row = index.row();
    TableRow songData;
    songData.albumPreview = songs[row].preview();
    songData.title = songs[row].name();
    songData.artist = songs[row].artist();
    songData.duration = songs[row].duration();
    songData.rating = songs[row].rating();

    return songData;
}

// Метод сохранения таблицы в базу данных
bool DataModel::saveTableData(const QString &fileName)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(fileName);

    if (!db.open()) {
        qDebug() << "Error: connection with database failed";
        return false;
    }
    // Создаем таблицу, если ее нет
    QSqlQuery querySelect(db);
    QSqlQuery query(db);
    if (!query.exec("CREATE TABLE IF NOT EXISTS Songs ("
                    "Preview BLOB, "
                    "Name TEXT, "
                    "Artist TEXT, "
                    "Duration TEXT, "
                    "Rating INTEGER, "
                    "Id INTEGER)")) {
        qDebug() << "Error: failed to create table:" << query.lastError();
        return false;
    }

    //Решил пойти через транзакцию
    // Начало транзакции
    db.transaction();


    QString selectQuery = "SELECT Id FROM Songs WHERE Id = :id";
    querySelect.prepare(selectQuery);

    QString updateQuery = "UPDATE Songs SET "
                          "Preview = :preview, "
                          "Name = :name, "
                          "Artist = :artist, "
                          "Duration = :duration, "
                          "Rating = :rating, "
                          "Id = :id "
                          "WHERE Id = :id";
    query.prepare(updateQuery);

    QString insertQuery = "INSERT INTO Songs (Preview, Name, Artist, Duration, Rating, Id) "
                          "VALUES (:preview, :name, :artist, :duration, :rating, :id)";
    query.prepare(insertQuery);

    for (int row = 0; row < rowCount(); ++row) {
        int id = 0;
        // Запрос на существование записи с данным ID
        querySelect.bindValue(":id", songs[row].id());
        
        if (querySelect.exec() && querySelect.next()) {
            id = querySelect.value(0).toInt();
        } else {
            qDebug() << "ID not found. Error executing SELECT query:" << querySelect.lastError().text();
        }

        // Подготовка запроса в зависимости от наличия записи в БД по ID
        if (id != 0) {
            query.prepare(updateQuery);
        } else {
            query.prepare(insertQuery);
        }

        saveTableRowData(query, row);

        // Выполнение запроса
        if (!query.exec()) {
            qDebug() << "Error executing query:" << query.lastError().text();
            // Откат транзакции в случае ошибки
            db.rollback();
            db.close();
            return false;
        }
    }

    // Завершение транзакции
    db.commit();
    
    db.close();
    return true;
}


//Метод загрузки сохранённой таблицы
bool DataModel::loadTableData(const QString &fileName)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(fileName);

    if (!db.open()) {
        qDebug() << "Failed to open database";
        return false;
    }

    QSqlQuery query;
    query.prepare("SELECT * FROM Songs"); // Делаем запрос к БД

    if (!query.exec()) {
        qDebug() << "Failed to execute query";
        db.close();
        return false;
    }

    // Очищаем текущие данные в модели
    beginResetModel();

    songs.clear();

    while (query.next())
    {
        QPixmap pixmap;
        QByteArray byteArray = query.value("Preview").toByteArray();
        pixmap.loadFromData(byteArray, "PNG");

        QString name = query.value("Name").toString();
        QString artist = query.value("Artist").toString();
        QString duration = query.value("Duration").toString();
        int rating = query.value("Rating").toInt();

        TableRow row;
        row.albumPreview = pixmap;
        row.title = name;
        row.artist = artist;
        row.duration = duration;
        row.rating = rating;

        insertRow(0, 1, row, QModelIndex());
    }

    db.close();
    endResetModel();
    return true;
}

void DataModel::saveTableRowData(QSqlQuery &query, int row)
{
    // Преобразование изображения в массив байтов (формат PNG) для сохранения в формате BLOB
    QByteArray byteArray;
    QBuffer buffer(&byteArray);
    buffer.open(QIODevice::WriteOnly);
    songs[row].preview().save(&buffer, "PNG");

    // Привязка значений к параметрам запроса с использованием addBindValue
    query.bindValue(":preview", byteArray);//QVariant::fromValue(byteArray));
    query.bindValue(":name", songs[row].name());
    query.bindValue(":artist", songs[row].artist());
    query.bindValue(":duration", songs[row].duration());
    query.bindValue(":rating", songs[row].rating());
    query.bindValue(":id", songs[row].id());
}



bool DataModel::editRow(int row, int count, TableRow row1)
{
    songs[row].setValues(row1);
    return true;
}
