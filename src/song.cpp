#include "song.h"
#include "src/tablerow.h"

Song::Song(const QPixmap& preview, const QString& name, const QString& artist, const QString& duration, int rating)
    : m_preview(preview), m_name(name), m_artist(artist), m_duration(duration), m_rating(rating)
{
    static int idCounter = 1;
    m_ID = idCounter++; // Генерируем новый идентификатор при создании объекта
}

QPixmap Song::preview() const {
    return m_preview;
}

QString Song::name() const {
    return m_name;
}

QString Song::artist() const {
    return m_artist;
}

QString Song::duration() const {
    return m_duration;
}

int Song::rating() const {
    return m_rating;
}

int Song::id() const
{
    return m_ID;
}

void Song::setValues(TableRow row)
{
    m_preview = row.albumPreview;
    m_name = row.title;
    m_artist = row.artist;
    m_duration = row.duration;
    m_rating = row.rating;
}
