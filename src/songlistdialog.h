#pragma once
#include <QDialog>
#include "src/datamodel.h"
#include "tablerow.h"
#include <QSortFilterProxyModel>

namespace Ui 
{
class SongListDialog;
}


class SongListDialog : public QDialog 
{
Q_OBJECT
private:
    int m_currentRowIndex;
    QString fileName = "SavedSongs.db"; // Имя файла, из которого загружаем данные таблицы
    DataModel *m_model; //Создаём экземпляр класса модели данных
    QSortFilterProxyModel *proxyModel; // Объект прокси-модели
    void insertRow(TableRow row, int m_currentRowIndex);
    Ui::SongListDialog* m_ui = nullptr;
    void saveTableData(const QString& fileName);
    void loadTableData(const QString& fileName);

public:
	SongListDialog(QWidget *parent = nullptr);
	~SongListDialog();

private slots:
    void on_btnCreate_clicked();
    void on_btnEdit_clicked();
    void on_btnSort_clicked();
    void on_btnSave_clicked();
};
